package com.example.loser.myapplication;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    private MainActivity classObj;

    @Before
    public void setup(){
        classObj = new MainActivity();
    }

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        boolean result = classObj.isStringSame("anu","kut");
        assertFalse(result);
    }
}