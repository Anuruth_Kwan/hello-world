package com.example.loser.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText userName;
    private EditText password;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        bindView();
        initView();
    }

    private void initView(){
        button.setOnClickListener(this);
    }

    private void bindView() {
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.passWord);
        button = findViewById(R.id.button);
    }
    public boolean isStringSame(String input, String compare){
        return input.equals(compare);
    }

    @Override
    public void onClick(View view) {

        userName.setText(password.getText());

        if(isStringSame(userName.getText().toString(),"anuruth")){
            Toast.makeText(MainActivity.this, "username is correct", Toast.LENGTH_SHORT).show();
        }

    }
}
